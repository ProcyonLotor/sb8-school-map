@ECHO OFF
:: Sets up a local VPROJECT for the addon

IF NOT EXIST "%~1\gameinfo.txt" (
	ECHO ** ERROR: Game folder "%~1" has no "gameinfo.txt", the path is invalid for VPROJECT.
	PAUSE & EXIT /B 1
)

SETLOCAL
SET "VPROJECT_PATH=%~dp0.vproject"
PUSHD "%~1"
:: Get absolute path and set VPROJECT
ECHO %CD%> "%VPROJECT_PATH%"
ECHO Successfully set "%CD%" as local VPROJECT in "%VPROJECT_PATH%"
POPD
ENDLOCAL

PAUSE & EXIT /B 0
