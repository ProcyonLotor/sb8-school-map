@ECHO OFF
SETLOCAL EnableDelayedExpansion
::
:: Cleans the working directory
::
:: Usage by user:
::     >cleanup [/Y]

PUSHD "%WORKDIR%" || ( ECHO WORKDIR=%WORKDIR% & EXIT /B )
IF /I NOT "%~1"=="/Y" (
	git clean -dfX -e ^^!.vproject -e ^^!*.vmm_prefs --dry-run || EXIT /B
	ECHO.
	CHOICE /M "Continue?"
	IF !ERRORLEVEL! NEQ 1 (
		ECHO Aborting working directory clean up...
		GOTO end
	)
)
git clean -dfX -e ^^!.vproject -e ^^!*.vmm_prefs || EXIT /B
POPD

:end
ENDLOCAL
EXIT /B 0
