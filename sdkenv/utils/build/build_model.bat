@ECHO OFF
::
:: Wrapper script for models compiling
::
:: Usage:
::     CALL build_model.bat <qc_file>
::
:: Parameters:
::     qc_file - model source file

:: Check if input is valid
IF NOT "%~x1"==".qc" (
	ECHO ** ERROR: Invalid QC file.
	EXIT /B 1
)

SETLOCAL
SET "QC_FILE=%~dpnx1"

:: Model compiling
ECHO.
CALL studiomdl "%QC_FILE%" || EXIT /B

:: If WORKDIR is the same folder as VPROJECT we don't need to move anything
IF "%WORKDIR%"=="%VPROJECT%" (
	ECHO * WORKDIR is the VPROJECT, don't need to move anything.
	EXIT /B 0
)

:: Find "$modelname" value in QC file
FOR /F "tokens=1*" %%A IN ('FINDSTR /I /C:"$modelname" "%QC_FILE%"') DO (
	IF /I "%%~A"=="$modelname" (
		SET "MODELNAME=%%~B"
	)
)

:: If "$modelname" was not found, there's probably some fuck up here
IF NOT DEFINED MODELNAME (
	ECHO ** ERROR: Cannot find modelname in "%QC_FILE%", move compiled model's files manually.
	EXIT /B 1
)

:: Normalize slashes in path
SET "MODELNAME=%MODELNAME:/=\%"
:: Absolute filename of compiled model, without extension
FOR %%I IN ("%VPROJECT%\models\%MODELNAME%") DO ( SET "MDL_NAME=%%~dpnI" )
:: Model's destination folder in addon's directory
FOR %%I IN ("%WORKDIR%\models\%MODELNAME%") DO ( SET "MDL_DEST=%%~dpI" )

:: Move compiled files to addon directory
ECHO Moving model files home...
IF NOT EXIST "%MDL_DEST%\" ( MKDIR "%MDL_DEST%" || ( ECHO ** ERROR: Cannot create directory "%OUTPUT_DIR%". & EXIT /B ) )
MOVE /Y "%MDL_NAME%.*" "%MDL_DEST%" >nul && ECHO Done. || ( ECHO ** ERROR: Cannot move files "%MDL_NAME%.*" to "%MDL_DEST%". & EXIT /B )

ENDLOCAL
EXIT /B 0
