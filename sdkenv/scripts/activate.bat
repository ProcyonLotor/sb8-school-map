@ECHO OFF
::
:: Sets SDK environment for the shell
::
:: Usage by user:
::     >sdkenv\scripts\activate.bat

:: Set up SDK environment
PUSHD "%~dp0..\utils"
CALL "%CD%\check_sdk_env.bat" || ( POPD & EXIT /B )
POPD

:: Set up shell $PATH
IF DEFINED _OLD_PATH SET "PATH=%_OLD_PATH%"
SET "_OLD_PATH=%PATH%"
SET "PATH=%SDKENV%\scripts;%SDKENV%\tools;%PATH%"

:: Set up shell
IF NOT DEFINED PROMPT SET "PROMPT=$P$G"
IF DEFINED _OLD_PROMPT SET "PROMPT=%_OLD_PROMPT%"
SET "_OLD_PROMPT=%PROMPT%"
SET "PROMPT=(%ADDON_NAME%) %PROMPT%"
