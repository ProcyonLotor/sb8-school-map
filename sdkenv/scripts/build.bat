@ECHO OFF
::
:: Routing script for Assets Build job
:: This script gathers input from user and pass it to corresponding scripts
::
:: Usage by user:
::     >build [/?]
::     >build all <assets_type>
::     >build texture <tga_texture | texture_config>
::     >build material <vmt_material_file>
::     >build model <qc_model_file>
::     >build map <vmf_map_file> [/C compile_config] [/R [cubemap_iterations]]
::     >build cubemaps <bsp_map_file> [/C build_config] [/Y] [/R [cubemap_iterations]]

SETLOCAL
SET "ASSET_TYPE=%~1"
SET "ASSET_FILE=%~dpnx2"
IF NOT DEFINED ASSET_TYPE ( GOTO help )

:: Early routing to "build all", otherwise parsing optional parameters will fuck up the input
IF /I "%ASSET_TYPE%"=="all" GOTO all

:: Optional parameters parsing
SET "CUBEMAPS=0"
:loop
IF NOT "%~1"=="" (
	IF /I "%~1"=="/C" ( SET "COMPILE_CONFIG=%~2" & SHIFT )
	IF /I "%~1"=="/R" (
		ECHO %~2| FINDSTR /R "^[0-9]*$" >nul && (
			SET /A "CUBEMAPS=%~2" & SHIFT
		) || (
			SET "CUBEMAPS=1"
		)
	)
	IF /I "%~1"=="/Y" ( SET "NO_PROMPT=noprompt" )
	IF /I "%~1"=="/?" ( GOTO help )
	SHIFT & GOTO loop
)

:: Routing to corresponding build job
GOTO %ASSET_TYPE% 2>nul || ECHO ** ERROR: Unknown asset type "%ASSET_TYPE%".

::
:: Single asset build
::
:all
	CALL "%SDKENV%\utils\build_all\build_all.bat" "%~2" "%~3" "%~4" "%~5" "%~6" "%~7" "%~8" "%~9" || EXIT /B
	GOTO end

:texture
	CALL "%SDKENV%\utils\build\build_texture.bat" "%ASSET_FILE%" || EXIT /B
	GOTO end

:material
	CALL "%SDKENV%\utils\build\build_material.bat" "%ASSET_FILE%" || EXIT /B
	GOTO end

:model
	CALL "%SDKENV%\utils\build\build_model.bat" "%ASSET_FILE%" || EXIT /B
	GOTO end

:map
	CALL "%SDKENV%\utils\build\build_map.bat" "%ASSET_FILE%" "%COMPILE_CONFIG%" "%CUBEMAPS%" || EXIT /B
	GOTO end

:cubemaps
	CALL "%SDKENV%\utils\build\build_cubemaps.bat" "%ASSET_FILE%" "%COMPILE_CONFIG%" "%CUBEMAPS%" "%NO_PROMPT%" || EXIT /B
	GOTO end

:help
	ECHO.
	ECHO Compile assets
	ECHO.
	ECHO Usage:
	ECHO     build ^<asset_type^> ^<asset_file^> [build_options]
	ECHO.
	ECHO Examples:
	ECHO     build [/?]
	ECHO     build all [/?]
	ECHO     build all ^<assets_type^>
	ECHO     build texture ^<tga_texture ^| texture_config^>
	ECHO     build material ^<vmt_material_file^>
	ECHO     build model ^<qc_model_file^>
	ECHO     build map ^<vmf_map_file^> [/C compile_config] [/R [cubemap_iterations]]
	ECHO     build cubemaps ^<bsp_map_file^> [/C build_config] [/R [cubemap_iterations]] [/Y]
	GOTO end

:end
ENDLOCAL
EXIT /B 0
