@ECHO OFF
SETLOCAL
::
:: Wrapper script for building cubemaps into a compiled map
::
:: Usage (parameters are strictly positional):
::     CALL build_cubemaps.bat <INPUT_FILE> [default | ldr | hdr | both] [iterations] [noprompt]
::
:: Parameters:
::     INPUT_FILE - compiled map file
::     default    - builds cubemaps as is, doesn't alter any game settings
::     ldr        - builds in LDR mode only (sets "mat_hdr_level" to "0")
::     hdr        - builds in LDR mode only (sets "mat_hdr_level" to "2")
::     both       - builds in both LDR and HDR (on first run "mat_hdr_level" is set to "0", on second to "2")
::     iterations - number of cubemaps iterations
::     noprompt   - supress all prompts

:: Check if input file is valid
IF NOT "%~x1"==".bsp" (
	ECHO ** ERROR: Invalid compiled map file.
	EXIT /B 1
)

IF %~3 LSS 1 (
	ECHO.
	ECHO Cubemaps iterations is %~3, skipping.
	EXIT /B 0
)
SET "INPUT_FILE=%~dpnx1"
SET "MAP_NAME=%~n1"
SET "MAPS_DIR=%VPROJECT%\maps"
SET "MAP_FILE=%MAPS_DIR%\%MAP_NAME%.bsp"
SET "FINAL_MAP_FILE=%WORKDIR%\maps\%MAP_NAME%.bsp"
SET "ITERATIONS=%~3"
IF /I "%~4"=="noprompt" ( SET NO_PROMPT=1 )

:: Calculate minimal window resolution
IF NOT DEFINED CUBEMAP_MAX_SIZE ( SET "CUBEMAP_MAX_SIZE=256" )
SET /A "WINDOW_SIZE=%CUBEMAP_MAX_SIZE% * 4"
IF %WINDOW_SIZE% LSS 256 ( SET "WINDOW_SIZE=256" )

SET "MAP_MODE=%~2"
IF NOT DEFINED MAP_MODE ( SET "MAP_MODE=default" )
GOTO %MAP_MODE% 2>nul || ECHO ** ERROR: Cubemap configuration "%MAP_MODE%" not found.

::
:: Configurations
::
:default
	CALL :prepare || EXIT /B
	ECHO.
	ECHO Building cubemaps, please wait...
	CALL game -buildcubemaps %ITERATIONS% -window -w %WINDOW_SIZE% -h %WINDOW_SIZE% +map "%MAP_NAME%" || GOTO game_error
	GOTO end

:ldr
	CALL :prepare || EXIT /B
	ECHO.
	ECHO Building only LDR cubemaps, please wait...
	CALL game -buildcubemaps %ITERATIONS% -window -w %WINDOW_SIZE% -h %WINDOW_SIZE% +mat_hdr_level 0 +map "%MAP_NAME%" || GOTO game_error
	GOTO end

:hdr
	CALL :prepare || EXIT /B
	ECHO.
	ECHO Building only HDR cubemaps, please wait...
	CALL game -buildcubemaps %ITERATIONS% -window -w %WINDOW_SIZE% -h %WINDOW_SIZE% +mat_hdr_level 2 +map "%MAP_NAME%" || GOTO game_error
	GOTO end

:both
	CALL :prepare || EXIT /B
	ECHO.
	ECHO Building LDR cubemaps, please wait...
	CALL game -buildcubemaps %ITERATIONS% -window -w %WINDOW_SIZE% -h %WINDOW_SIZE% +mat_hdr_level 0 +map "%MAP_NAME%" || GOTO game_error
	ECHO Building HDR cubemaps, please wait...
	CALL game -buildcubemaps %ITERATIONS% -window -w %WINDOW_SIZE% -h %WINDOW_SIZE% +mat_hdr_level 2 +map "%MAP_NAME%" || GOTO game_error
	GOTO end

:end
MOVE /Y "%MAP_FILE%" "%INPUT_FILE%" >nul || ( ECHO ** ERROR: Cannot move file "%MAP_FILE%" back to "%INPUT_FILE%". & EXIT /B )
ECHO.
ECHO Cubemaps finished.
ENDLOCAL
EXIT /B 0

:game_error
MOVE /Y "%MAP_FILE%" "%INPUT_FILE%" >nul || ECHO ** ERROR: Cannot move file "%MAP_FILE%" back to "%INPUT_FILE%".
ECHO ** ERROR: The game crashed while building cubemaps for "%INPUT_FILE%".
ENDLOCAL
EXIT /B 1


::
:: Subroutines
::
:prepare
	SETLOCAL EnableDelayedExpansion

	ECHO.
	ECHO Start building cubemaps for "%INPUT_FILE%" with config "%MAP_MODE%" and %ITERATIONS% iterations...

	:: Confirm bspzip -deletecubemaps
	IF NOT DEFINED NO_PROMPT (
		ECHO.
		CHOICE /M "** HOLD ON^! You're going to build cubemaps, this will remove all embeded texture files(.vtf) from .bsp, continue?"
		IF !ERRORLEVEL! NEQ 1 (
			ECHO Aborting cubemaps building...
			EXIT /B
		)
	)

	:: Checking that nothing will interfere with cubemaps building
	IF EXIST "%FINAL_MAP_FILE%" (
		IF /I NOT "%FINAL_MAP_FILE%"=="%INPUT_FILE%" (
			IF NOT DEFINED NO_PROMPT (
				ECHO.
				CHOICE /M "** WARNING: A BSP file with same name (%MAP_NAME%.bsp) is already present in compiled maps directory (%FINAL_MAP_FILE%), it WILL interfere with cubemaps building, delete this file?"
				IF !ERRORLEVEL! NEQ 1 (
					ECHO Aborting cubemaps building...
					EXIT /B
				)
			)
			DEL /F "%FINAL_MAP_FILE%" || EXIT /B
		)
	)

	:: Removing default cubemaps
	CALL bspzip -deletecubemaps "%INPUT_FILE%" || ( ECHO ** ERROR: Cannot remove default cubemaps in "%INPUT_FILE%". & EXIT /B )
	:: Some Source games can't build cubemaps into a map from custom location, unless it placed in "<game_dir>\maps"
	IF NOT EXIST "%MAPS_DIR%\" ( MKDIR "%MAPS_DIR%" || ( ECHO ** ERROR: Cannot create directory "%MAPS_DIR%". & EXIT /B ) )
	MOVE /Y "%INPUT_FILE%" "%MAP_FILE%" >nul || ( ECHO ** ERROR: Cannot move file "%INPUT_FILE%" to "%MAP_FILE%". & EXIT /B )

	ENDLOCAL
	EXIT /B 0
