@ECHO OFF
::
:: Checks and sets SDK environment
:: Call this script form other scripts when checking the environment!
::
:: Usage:
::     CALL check_sdk_env.bat

:: Checking if VPROJECT is already set.
IF NOT DEFINED VPROJECT (
	CALL "%~dp0set_sdk_env.bat" || EXIT /B
) ELSE (
	CALL "%~dp0check_env_integrity.bat" || EXIT /B
)

EXIT /B 0
