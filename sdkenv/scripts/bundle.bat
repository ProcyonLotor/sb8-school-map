@ECHO OFF
SETLOCAL
::
:: Routing script for Bundling job
::
:: Usage by user:
::     >bundle [/?]
::     >bundle folder [output_dir_path]
::     >bundle vpk [output_file]
::     >bundle bsp <bsp_file> [output_file]

:: Routing to corresponding bundling job
SET "BUNDLE_TYPE=%~1"
IF NOT DEFINED BUNDLE_TYPE ( GOTO help )
IF "%BUNDLE_TYPE%"=="/?" ( GOTO help )
GOTO %BUNDLE_TYPE% 2>nul || ECHO ** ERROR: Unknown bundle type "%BUNDLE_TYPE%".

::
:: Bundling configurations
::
:folder
	CALL "%SDKENV%\utils\bundle\bundle_dir.bat" "%~2" "%~3" "%~4" "%~5" "%~6" "%~7" "%~8" "%~9" || EXIT /B
	GOTO end

:vpk
	CALL "%SDKENV%\utils\bundle\bundle_vpk.bat" "%~2" "%~3" "%~4" "%~5" "%~6" "%~7" "%~8" "%~9" || EXIT /B
	GOTO end

:bsp
	CALL "%SDKENV%\utils\bundle\bundle_bsp.bat" "%~2" "%~3" "%~4" "%~5" "%~6" "%~7" "%~8" "%~9" || EXIT /B
	GOTO end

:gma
	CALL "%SDKENV%\utils\bundle\bundle_gma.bat" "%~2" "%~3" "%~4" "%~5" "%~6" "%~7" "%~8" "%~9" || EXIT /B
	GOTO end

:help
	ECHO.
	ECHO Assets bundling
	ECHO.
	ECHO Usage:
	ECHO     bundle ^<config^> [additional_options]
	ECHO.
	ECHO Examples:
	ECHO     bundle folder [output_dir_path]
	ECHO     bundle vpk [output_file]
	ECHO     bundle bsp ^<bsp_file^> [output_file]
	ECHO.
	ECHO Configs:
	ECHO     folder - copy all compilied content to a folder
	ECHO     vpk    - pack addon content into .vpk file
	ECHO     bsp    - embed content into .bsp file
	GOTO end

:end
ENDLOCAL
EXIT /B 0
