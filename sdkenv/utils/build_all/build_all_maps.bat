@ECHO OFF
SETLOCAL EnableDelayedExpansion
::
:: Script for batch maps compiling
::
:: Usage (parameters are strictly positional):
::     CALL build_all_maps.bat [search_path] [compile_config] [cubemaps] [noprompt]
::
:: Parameters:
::     search_path    - path to a directory with .vmf files to compile, if not specified, compiles everything in "%WORKDIR%\mapsrc" folder
::     compile_config - map compile configuration (default, fast, hdr, final, onlyents)
::     cubemaps       - number of cubemaps iterations
::     noprompt       - supress all prompts

SET "SEARCH_PATH=%~dpnx1"
SET "COMPILE_CONFIG=%~2"

:: Optional parameters
IF /I "%~4"=="noprompt" ( SET NO_PROMPT=1 )

ECHO.
ECHO Batch maps compilation started

:: Input validation and routing
IF NOT DEFINED SEARCH_PATH (
	:: By default, compile everything in addon directory
	SET "SEARCH_PATH=%WORKDIR%\mapsrc\"
	IF NOT EXIST "!SEARCH_PATH!" (
		ECHO ** ERROR: Directory "mapsrc" doesn't exist in WORKDIR.
		EXIT /B 1
	)
) ELSE (
	IF NOT EXIST "!SEARCH_PATH!" (
		ECHO ** ERROR: Specified directory "%SEARCH_PATH%" doesn't exist.
		EXIT /B 1
	)
	:: Remove trailing slashes
	IF "!SEARCH_PATH:~-1!"=="\" ( SET "SEARCH_PATH=!SEARCH_PATH:~0,-1!" )
)

IF NOT DEFINED NO_PROMPT (
	ECHO.
	CHOICE /M "** HOLD ON^! You're going to compile several maps in a row, this might take a VERY long time, are you sure?"
	IF !ERRORLEVEL! NEQ 1 (
		ECHO Aborting batch maps compilation...
		EXIT /B 0
	)
)

:: Looking for .vmf and .vmm files in specified folder
FOR %%I IN ("!SEARCH_PATH!\*.vmf","!SEARCH_PATH!\*.vmm") DO (
	CALL "%SDKENV%\utils\build\build_map.bat" "%%~I" "%COMPILE_CONFIG%" "%~3" || EXIT /B
)

:end
ECHO.
ECHO Batch maps compilation finished.
ENDLOCAL
EXIT /B 0
