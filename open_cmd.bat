@ECHO OFF
:: Open command prompt with set up environment

:: Check if local VPROJECT is set up
IF EXIST "%~dp0.vproject" (
	CALL "%~dp0sdkenv\scripts\activate.bat" || ( PAUSE & EXIT /B )
) ELSE (
	ECHO ** ERROR: Cannot find local VPROJECT, try run "set_vproject <path_to_mod>" first.
)

CD /D "%~dp0"
CMD /K
