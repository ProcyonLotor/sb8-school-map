@ECHO OFF
::
:: Script for batch materials compiling
::
:: Usage:
::     CALL build_all_materials.bat [dir_path]
::
:: Parameters:
::     dir_path - path to a directory with material files to compile,
::                if not specified, compiles every .vmt file in "%WORKDIR%\materialsrc" folder
SETLOCAL EnableDelayedExpansion
SET "SEARCH_PATH=%~dpnx1"

ECHO.
ECHO Batch materials compilation started

:: Input validation and routing
IF NOT DEFINED SEARCH_PATH (
	:: By default, compile everything in addon directory
	SET "SEARCH_PATH=%WORKDIR%\materialsrc\"
	IF NOT EXIST "!SEARCH_PATH!" (
		ECHO ** ERROR: Directory "materialsrc" doesn't exist in WORKDIR.
		EXIT /B 1
	)
	GOTO addon_dir
) ELSE (
	IF NOT EXIST "!SEARCH_PATH!" (
		ECHO ** ERROR: Specified directory "%SEARCH_PATH%" doesn't exist.
		EXIT /B 1
	)
	:: Remove trailing slashes
	IF "!SEARCH_PATH:~-1!"=="\" ( SET "SEARCH_PATH=!SEARCH_PATH:~0,-1!" )
	GOTO custom_dir
)

:: Looking for .vmt files in "%WORKDIR%\materialsrc" folder recursively
:addon_dir
	FOR /R "%SEARCH_PATH%" %%I IN (*.vmt) DO (
		CALL "%SDKENV%\utils\build\build_material.bat" "%%~I" || EXIT /B
	)
	GOTO end

:: Looking for .vmt files in specified folder only
:custom_dir
	FOR %%I IN ("!SEARCH_PATH!\*.vmt") DO (
		CALL "%SDKENV%\utils\build\build_material.bat" "%%~I" || EXIT /B
	)
	GOTO end

:end
ECHO.
ECHO Batch materials compilation finished.
ENDLOCAL
EXIT /B 0
