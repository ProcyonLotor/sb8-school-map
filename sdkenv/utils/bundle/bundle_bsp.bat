@ECHO OFF
SETLOCAL
::
:: Script for bundling content into the map
::
:: Usage (parameters are strictly positional):
::     CALL bundle_bsp.bat <bsp_file> [output_file]
::
:: Parameters:
::     bsp_file    - map into which content is packed
::     output_file - optional output file, by default it is the same as the <bsp_file>

:: What folders to pack
SET "CONTENT_DIRS=expressions;maps;materials;media;models;particles;resource;scenes;scripts;sound"
:: What files to look for
SET "CONTENT_TYPES=.ani;.bik;.cur;.dat;.jpg;.lmp;.lst;.mdl;.mov;.mp3;.nav;.pcf;.phy;.png;.res;.txt;.vcd;.vfe;.vfont;.vmt;.vtf;.vtx;.vvd;.wav"

:: Check if input file is valid
IF NOT "%~x1"==".bsp" (
	ECHO ** ERROR: Invalid map file.
	EXIT /B 1
)
IF NOT EXIST "%~1" (
	ECHO ** ERROR: Specified map file "%~dpnx1" doesn't exist.
	EXIT /B 1
)

:: Input parameters
SET "BSP_FILE=%~dpnx1"
SET "FILES_LIST=%WORKDIR%\bspzip-%~n1.txt"
SET "OUTPUT_FILE=%~dpnx2"
IF NOT DEFINED OUTPUT_FILE (
	SET "OUTPUT_FILE=%BSP_FILE%"
) ELSE (
	IF NOT EXIST "%~dp2" (
		MKDIR "%~dp2" || ( ECHO ** ERROR: Cannot create output directory "%~dp2". & EXIT /B )
	)
)

:: Preparing files list file
>"%FILES_LIST%" CALL :sweep_dirs || ( ECHO ** ERROR: Cannot create files list file "%FILES_LIST%". & EXIT /B )
:: Packing content into the map file
ECHO Packing content in "%OUTPUT_FILE%"...
CALL bspzip -addlist "%BSP_FILE%" "%FILES_LIST%" "%OUTPUT_FILE%" || EXIT /B
:: Clean up list file
DEL /F "%FILES_LIST%" || EXIT /B

ECHO Done BSP packing.
ENDLOCAL
EXIT /B 0


::
:: Subroutines
::
:sweep_dirs
	FOR %%A IN ("%CONTENT_DIRS:;=" "%") DO (
		CALL :find_files "%WORKDIR%\%%~A" || EXIT /B
	)
	EXIT /B 0

:find_files
	SETLOCAL EnableDelayedExpansion
	FOR /R "%~1" %%I IN ("*%CONTENT_TYPES:;=" "*%") DO (
		SET "ABS_PATH=%%~I"
		:: Path inside .bsp file
		ECHO !ABS_PATH:%WORKDIR%\=!
		:: Path to file on disk
		ECHO !ABS_PATH!
	)
	ENDLOCAL
	EXIT /B 0
