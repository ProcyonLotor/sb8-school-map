@ECHO OFF
SETLOCAL EnableDelayedExpansion
::
:: Routing script for Batch Assets Build job
:: Similar to build.bat, except this one handles input for Batch Compiling scripts
:: All input from build.bat just dumped into here
::
:: Usage by user:
::     >build all [/?]
::     >build all [/C map_compile_config] [/R [iterations]] [/Y]
::     >build all textures [/D textures_dir]
::     >build all materials [/D vmt_materials_dir]
::     >build all models [/D models_dir]
::     >build all maps [/D mapsrc_path] [/C map_compile_config] [/R [iterations]] [/Y]
::     >build all cubemaps [/D maps_path] [/C build_config] [/R [cubemap_iterations]] [/Y]
::
:: Usage in scripts:
::     CALL "build_all.bat" %*

SET "ASSETS_TYPE=%~1"
IF NOT DEFINED ASSETS_TYPE ( SET "ASSETS_TYPE=all" )
IF "%ASSETS_TYPE:~0,1%"=="/" ( SET "ASSETS_TYPE=all" )

:: Optional parameters parsing
SET "CUBEMAPS=0"
:loop
IF NOT "%~1"=="" (
	IF /I "%~1"=="/D" ( SET "SEARCH_PATH=%~dpnx2" & SHIFT )
	IF /I "%~1"=="/C" ( SET "COMPILE_CONFIG=%~2" & SHIFT )
	IF /I "%~1"=="/R" (
		ECHO %~2| FINDSTR /R "^[0-9]*$" >nul && (
			SET /A "CUBEMAPS=%~2" & SHIFT
		) || (
			SET "CUBEMAPS=1"
		)
	)
	IF /I "%~1"=="/Y" ( SET "NO_PROMPT=noprompt" )
	IF /I "%~1"=="/?" ( GOTO help )
	SHIFT & GOTO loop
)

:: Routing to corresponding build job
GOTO %ASSETS_TYPE% 2>nul || ECHO ** ERROR: Unknown assets type "%ASSETS_TYPE%".

::
:: Batch assets compilation
::
:all
	IF NOT DEFINED NO_PROMPT (
		ECHO.
		CHOICE /M "** HOLD ON^! You're going to compile EVERYTHING in the addon folder, this is will take a VERY long time, the maps especially, are you sure want to compile everything"
		IF !ERRORLEVEL! NEQ 1 (
			ECHO Aborting total addon compilation...
			EXIT /B 0
		)
	)
	SET "SEARCH_PATH="
	ECHO.
	ECHO Total addon compilation started

	:: Textures first, then materials
	IF EXIST "%WORKDIR%\materialsrc\" (
		CALL "%SDKENV%\utils\build_all\build_all_textures.bat" "%SEARCH_PATH%" || EXIT /B
		ECHO.
		CALL "%SDKENV%\utils\build_all\build_all_materials.bat" "%SEARCH_PATH%" || EXIT /B
	) ELSE (
		ECHO.
		ECHO * Directory "materialsrc" doesn't exist in WORKDIR, ignoring Textures and Materials build.
	)
	ECHO.

	:: After that, models
	IF EXIST "%WORKDIR%\modelsrc\" (
		CALL "%SDKENV%\utils\build_all\build_all_models.bat" "%SEARCH_PATH%" || EXIT /B
	) ELSE (
		ECHO.
		ECHO * Directory "modelsrc" doesn't exist in WORKDIR, ignoring Models build.
	)
	ECHO.

	:: Finally, maps
	IF EXIST "%WORKDIR%\mapsrc\" (
		CALL "%SDKENV%\utils\build_all\build_all_maps.bat" "%SEARCH_PATH%" "%COMPILE_CONFIG%" "%CUBEMAPS%" "noprompt" || EXIT /B
	) ELSE (
		ECHO.
		ECHO * Directory "mapsrc" doesn't exist in WORKDIR, ignoring Maps build.
	)
	ECHO.

	ECHO Total addon compilation finished.
	GOTO end

:textures
	CALL "%SDKENV%\utils\build_all\build_all_textures.bat" "%SEARCH_PATH%" || EXIT /B
	GOTO end

:materials
	CALL "%SDKENV%\utils\build_all\build_all_materials.bat" "%SEARCH_PATH%" || EXIT /B
	GOTO end

:models
	CALL "%SDKENV%\utils\build_all\build_all_models.bat" "%SEARCH_PATH%" || EXIT /B
	GOTO end

:maps
	CALL "%SDKENV%\utils\build_all\build_all_maps.bat" "%SEARCH_PATH%" "%COMPILE_CONFIG%" "%CUBEMAPS%" "%NO_PROMPT%" || EXIT /B
	GOTO end

:cubemaps
	CALL "%SDKENV%\utils\build_all\build_all_cubemaps.bat" "%SEARCH_PATH%" "%COMPILE_CONFIG%" "%CUBEMAPS%" "%NO_PROMPT%" || EXIT /B
	GOTO end

:help
	ECHO.
	ECHO Batch compiling assets
	ECHO.
	ECHO Usage:
	ECHO     build all [assets_type] [build_options]
	ECHO.
	ECHO Examples:
	ECHO     build all [/?]
	ECHO     build all [/C map_compile_config] [/R [cubemap_iterations]] [/Y]
	ECHO     build all textures [/D textures_dir]
	ECHO     build all materials [/D vmt_materials_dir]
	ECHO     build all models [/D models_dir]
	ECHO     build all maps [/D mapsrc_path] [/C map_compile_config] [/R [cubemap_iterations]] [/Y]
	ECHO     build all cubemaps [/D maps_path] [/C build_config] [/R [cubemap_iterations]] [/Y]
	GOTO end

:end
ENDLOCAL
EXIT /B 0
