@ECHO OFF
SETLOCAL
::
:: Automaticly run a command on a file change
:: Script whatches for when file changes and runs specified command
::
:: Usage by user:
::     >watch [/?]
::     >watch <target_file> "<command>"
:: * The command string must be surrounded by double quotes!

IF "%~1"=="/?" ( GOTO help )

:: Input checks
IF [%1] == [] (
	ECHO ** ERROR: Invalid target file.
	EXIT /B 1
)
IF [%2] == [] (
	ECHO ** ERROR: No command specified.
	EXIT /B 1
)
IF NOT EXIST "%~dpnx1" (
	ECHO ** ERROR: Target file "%~dpnx1" doesn't exist.
	EXIT /B 1
)

:: Input sanitization
SET "TARGET_FILE=%~dpnx1"
SET "CMD_STRING=%~2"

:: MOTD
ECHO.
ECHO Watching for changes in file: "%TARGET_FILE%"
ECHO Executing command "%CMD_STRING%" on update
ECHO.
ECHO Press CTRL + C to stop.
ECHO.

:: Main watch loop
CALL :get_timestamp "%TARGET_FILE%" LAST_TIME
:loop
	CALL :get_timestamp "%TARGET_FILE%" TIMESTAMP
	IF /I NOT "%TIMESTAMP%"=="%LAST_TIME%" (
		ECHO The File out of date. Rebuilding...
		ECHO.
		CALL :action "%CMD_STRING%"
		SET "LAST_TIME=%TIMESTAMP%"
		ECHO.
		ECHO Watching for changes...
	)
	:: Never build infinite loop without some idle time
	TIMEOUT /T 1 /NOBREAK >NUL
GOTO :loop

:end
ENDLOCAL
EXIT /B 0


::
:: Subroutines
::
:get_timestamp
	SETLOCAL
	SET "_FILE_DIR=%~dp1"
	SET "_FILE_DIR=%_FILE_DIR:~0,-1%"
	SET "_FILE_NAME=%~nx1"
	SET "_TIMESTAMP="
	FOR /F %%I IN ('FORFILES /P "%_FILE_DIR%" /M "%_FILE_NAME%" /C "CMD /C ECHO @FTIME"') DO (
		CALL SET "_TIMESTAMP=%%~I"
	)
	ENDLOCAL & SET "%~2=%_TIMESTAMP%"
	EXIT /B 0

:action
	CALL %~1
	EXIT /B 0

:help
	ECHO.
	ECHO Automaticly run a command on a file change
	ECHO Script whatches for when the file changes and runs a specified command
	ECHO.
	ECHO Usage:
	ECHO     ^>watch [/?]
	ECHO     ^>watch ^<target_file^> "<command>"*
	ECHO * The command string must be surrounded by double quotes!
	GOTO end
