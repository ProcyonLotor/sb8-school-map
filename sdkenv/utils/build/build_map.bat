@ECHO OFF
::
:: Wrapper script for Map compiling
::
:: Usage (parameters are strictly positional):
::     CALL build_map.bat <vmf_file> [compile_config] [cubemaps]
::
:: Parameters:
::     vmf_file       - map to compile
::     compile_config - name of one of compile configs ("default" used if empty)
::     cubemaps       - number of cubemaps iterations

:: Check if input file is valid
IF NOT "%~x1"==".vmf" (
	IF NOT "%~x1"==".vmm" (
		IF NOT "%~x1"==".vmx" (
			ECHO ** ERROR: Invalid map source file.
			EXIT /B 1
		)
	)
)

SETLOCAL
SET "MAP_NAME=%~n1"
SET "MAP_FILE=%~dpnx1"
SET "BSP_FILE=%~dpn1.bsp"
SET "MAPS_DIR=%WORKDIR%\maps"
SET "FINAL_BSP_FILE=%MAPS_DIR%\%MAP_NAME%.bsp"
SET "CUBEMAPS=%~3"

:: Routing to corresponding map compile configuration
SET "COMPILE_CONFIG=%~2"
IF NOT DEFINED COMPILE_CONFIG ( SET "COMPILE_CONFIG=default" )
GOTO %COMPILE_CONFIG% 2>nul || ECHO ** ERROR: Map compile configuration "%COMPILE_CONFIG%" not found.

::
:: Configurations
::
:default
	ECHO.
	ECHO Compiling map "%MAP_NAME%" with Default configuration...
	CALL vbsp "%MAP_FILE%" || EXIT /B
	CALL vvis "%BSP_FILE%" || EXIT /B
	CALL vrad "%BSP_FILE%" || EXIT /B
	IF %CUBEMAPS% GTR 0 (
		CALL "%SDKENV%\utils\build\build_cubemaps.bat" "%BSP_FILE%" "" "%CUBEMAPS%" noprompt || EXIT /B
	)
	CALL :finish || EXIT /B
	GOTO end

:fast
	ECHO.
	ECHO Compiling map "%MAP_NAME%" with Fast configuration...
	CALL vbsp "%MAP_FILE%" || EXIT /B
	CALL vvis -fast "%BSP_FILE%" || EXIT /B
	CALL vrad -bounce 2 -noextra "%BSP_FILE%" || EXIT /B
	IF %CUBEMAPS% GTR 0 (
		CALL "%SDKENV%\utils\build\build_cubemaps.bat" "%BSP_FILE%" "" "%CUBEMAPS%" noprompt || EXIT /B
	)
	CALL :finish || EXIT /B
	GOTO end

:hdr
	ECHO.
	ECHO Compiling map "%MAP_NAME%" with HDR Full configuration...
	CALL vbsp "%MAP_FILE%" || EXIT /B
	CALL vvis "%BSP_FILE%" || EXIT /B
	CALL vrad -both "%BSP_FILE%" || EXIT /B
	IF %CUBEMAPS% GTR 0 (
		CALL "%SDKENV%\utils\build\build_cubemaps.bat" "%BSP_FILE%" hdr "%CUBEMAPS%" noprompt || EXIT /B
	)
	CALL :finish || EXIT /B
	GOTO end

:final
	ECHO.
	ECHO Compiling map "%MAP_NAME%" with HDR Final configuration...
	CALL vbsp "%MAP_FILE%" || EXIT /B
	CALL vvis "%BSP_FILE%" || EXIT /B
	CALL vrad -ldr -final "%BSP_FILE%" || EXIT /B
	CALL vrad -hdr -final "%BSP_FILE%" || EXIT /B
	IF %CUBEMAPS% GTR 0 (
		CALL "%SDKENV%\utils\build\build_cubemaps.bat" "%BSP_FILE%" both "%CUBEMAPS%" noprompt || EXIT /B
	)
	CALL :finish || EXIT /B
	GOTO end

:onlyents
	ECHO.
	ECHO Compiling map "%MAP_NAME%" with Only Entities configuration...
	CALL vbsp -onlyents "%MAP_FILE%" || EXIT /B
	CALL :finish || EXIT /B
	GOTO end

:nolight
	ECHO.
	ECHO Compiling map "%MAP_NAME%" without VRAD...
	CALL vbsp "%MAP_FILE%" || EXIT /B
	CALL vvis "%BSP_FILE%" || EXIT /B
	IF %CUBEMAPS% GTR 0 (
		CALL "%SDKENV%\utils\build\build_cubemaps.bat" "%BSP_FILE%" "" "%CUBEMAPS%" noprompt || EXIT /B
	)
	CALL :finish || EXIT /B
	GOTO end

:end
ECHO.
ECHO Map compiled.
ENDLOCAL
EXIT /B 0


::
:: Subroutines
::
:finish
	IF NOT EXIST "%MAPS_DIR%\" ( MKDIR "%MAPS_DIR%" || ( ECHO ** ERROR: Cannot create directory "%MAPS_DIR%". & EXIT /B ) )
	COPY /Y "%BSP_FILE%" "%FINAL_BSP_FILE%" >nul || ( ECHO ** ERROR: Cannot copy file "%BSP_FILE%" to "%FINAL_BSP_FILE%". & EXIT /B )
	EXIT /B 0
