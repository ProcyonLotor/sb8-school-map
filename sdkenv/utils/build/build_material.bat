@ECHO OFF
::
:: Wrapper script for materials compiling
:: (actually just copying to "materials" folder)
::
:: Usage:
::     CALL build_material.bat <material_file>
::
:: Parameters:
::     material_file - nuff said.

:: Check if input is valid
IF NOT "%~x1"==".vmt" (
	ECHO ** ERROR: Invalid material file.
	EXIT /B 1
)

SETLOCAL
SET "INPUT_FILE=%~dpnx1"
SET "INPUT_DIR=%~dp1"

:: Removing trailing slashes
IF "%INPUT_DIR:~-1%"=="\" ( SET "INPUT_DIR=%INPUT_DIR:~0,-1%" )

:: Checking if we can resolve final place in "materials" folder
IF /I "%INPUT_DIR:materialsrc=%"=="%INPUT_DIR%" (
	ECHO ** ERROR: Material file is in wrong place, put it inside "materialsrc" in mod's directory.
	EXIT /B 1
)

:: Figuring out material path realtive to "materials" folder
FOR %%A IN ("%INPUT_DIR:\=" "%") DO (
	CALL SET "MAT_PATH=%%MAT_PATH%%\%%~A"
	IF /I "%%~A"=="materialsrc" ( CALL SET "MAT_PATH=" )
)
SET "OUTPUT_DIR=%WORKDIR%\materials%MAT_PATH%"

:: Copying to material file
IF NOT EXIST "%OUTPUT_DIR%\" ( MKDIR "%OUTPUT_DIR%" || ( ECHO ** ERROR: Cannot create directory "%OUTPUT_DIR%". & EXIT /B ) )
ECHO.
ECHO Copying: "%INPUT_FILE%"
ECHO      To: "%OUTPUT_DIR%"
COPY /Y "%INPUT_FILE%" "%OUTPUT_DIR%" >nul || ( ECHO ** ERROR: Cannot copy file "%INPUT_FILE%" to "%OUTPUT_DIR%". & EXIT /B )

ENDLOCAL
EXIT /B 0
