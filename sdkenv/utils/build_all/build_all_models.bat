@ECHO OFF
::
:: Script for batch models compiling
::
:: Usage:
::     CALL build_all_models.bat [dir_path]
::
:: Parameters:
::     dir_path - path to a directory with models to compile,
::                if not specified, compiles everything in "%WORKDIR%\modelsrc" folder
SETLOCAL EnableDelayedExpansion
SET "SEARCH_PATH=%~dpnx1"

ECHO.
ECHO Batch models compilation started

:: Input validation and routing
IF NOT DEFINED SEARCH_PATH (
	:: By default, compile everything in addon directory
	SET "SEARCH_PATH=%WORKDIR%\modelsrc\"
	IF NOT EXIST "!SEARCH_PATH!" (
		ECHO ** ERROR: Directory "modelsrc" doesn't exist in WORKDIR.
		EXIT /B 1
	)
	GOTO addon_dir
) ELSE (
	IF NOT EXIST "!SEARCH_PATH!" (
		ECHO ** ERROR: Specified directory "%SEARCH_PATH%" doesn't exist.
		EXIT /B 1
	)
	:: Remove trailing slashes
	IF "!SEARCH_PATH:~-1!"=="\" ( SET "SEARCH_PATH=!SEARCH_PATH:~0,-1!" )
	GOTO custom_dir
)

:: Looking for .qc files in "%WORKDIR%\modelsrc" folder recursively
:addon_dir
	FOR /R "%SEARCH_PATH%" %%I IN (*.qc) DO (
		CALL "%SDKENV%\utils\build\build_model.bat" "%%~I" || EXIT /B
	)
	GOTO end

:: Looking for .qc files in specified folder only
:custom_dir
	FOR %%I IN ("!SEARCH_PATH!\*.qc") DO (
		CALL "%SDKENV%\utils\build\build_model.bat" "%%~I" || EXIT /B
	)
	GOTO end

:end
ECHO.
ECHO Batch models compilation finished.
ENDLOCAL
EXIT /B 0
