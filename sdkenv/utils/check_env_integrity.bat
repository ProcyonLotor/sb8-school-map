@ECHO OFF
SETLOCAL EnableDelayedExpansion
::
:: Checks for environment variables integrity
::
:: Usage:
::     CALL check_env_integrity.bat

:: List of SDK environment variables to check
:: should be same as in "set_sdk_env.bat"
FOR %%V IN (SDKENV WORKDIR VPROJECT) DO (
	IF NOT DEFINED %%V (
		ECHO ** ERROR: Variable "%%V" listed but not defined.
		EXIT /B 1
	)
	IF NOT EXIST "!%%V!" (
		ECHO ** ERROR: Variable "%%V=!%%V!" defined but doesn't exist.
		EXIT /B 1
	)
)

:: Check tools integrity
FOR /F "tokens=1 delims==" %%V IN ('FINDSTR /B /V /R /C:" *#" "%SDKENV%\tools.txt"') DO (
	IF NOT EXIST "!%%V!" (
		ECHO ** WARNING: Tool "%%V=!%%V!" is defined but doesn't exist, some functionality might be limited.
	)
)

ECHO SDK environment is OK.

EXIT /B 0
