@ECHO OFF
SETLOCAL
::
:: Script for bundling content into .GMA file
::
:: Usage:
::     CALL bundle_gma.bat [output_gma]
::
:: Parameters:
::     output_gma - optional output file, by default it is "%WORKDIR%\dist\%ADDON_NAME%.gma"

SET "ADDON_DIR=%WORKDIR%\dist\%ADDON_NAME%"

SET "ADDON_GMA=%~dpnx1"
IF NOT DEFINED ADDON_GMA ( SET "ADDON_GMA=%WORKDIR%\dist\%ADDON_NAME%.gma" )

SET "ADDON_JSON=%WORKDIR%\addon.json"
IF NOT EXIST "%ADDON_JSON%" (
	ECHO ** ERROR: "addon.json" doesn't exist! Cannot create .gma without it.
	EXIT /B 1
)

:: Extract whitelisted files, see https://wiki.facepunch.com/gmod/Workshop_Addon_Creation#folderwithaddonfiles
CALL "%SDKENV%\utils\bundle\bundle_dir.bat" "%ADDON_DIR%" || EXIT /B

:: Copying addon.json
ECHO.
ECHO Copying: "%ADDON_JSON%"
ECHO      To: "%ADDON_DIR%"
COPY /Y "%ADDON_JSON%" "%ADDON_DIR%" >nul || ( ECHO ** ERROR: Cannot copy file "%ADDON_JSON%" to "%ADDON_DIR%". & EXIT /B )
ECHO.

:: Create .gma file
CALL gmad create -folder "%ADDON_DIR%" -out "%ADDON_GMA%" || EXIT /B

ECHO Done GMod addon packing.
ENDLOCAL
EXIT /B 0
