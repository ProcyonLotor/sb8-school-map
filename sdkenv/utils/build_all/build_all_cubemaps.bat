@ECHO OFF
SETLOCAL EnableDelayedExpansion
::
:: Script for batch cubemaps building
::
:: Usage (parameters are strictly positional):
::     CALL build_all_cubemaps.bat [search_path] [compile_config] [iterations] [noprompt]
::
:: Parameters:
::     search_path    - path to a directory with .bsp files to build cubemaps for, if not specified, compiles everything in "%WORKDIR%\maps" folder
::     compile_config - build configuration (default, ldr, hdr, both)
::     iterations     - number of cubemaps iterations
::     noprompt       - supress all prompts

SET "SEARCH_PATH=%~dpnx1"
SET "COMPILE_CONFIG=%~2"

:: Optional parameters
IF /I "%~4"=="noprompt" ( SET NO_PROMPT=1 )

ECHO.
ECHO Batch cubemaps building started

:: Input validation and routing
IF NOT DEFINED SEARCH_PATH (
	:: By default, compile everything in addon directory
	SET "SEARCH_PATH=%WORKDIR%\maps\"
	IF NOT EXIST "!SEARCH_PATH!" (
		ECHO ** ERROR: Directory "maps" doesn't exist in WORKDIR.
		EXIT /B 1
	)
) ELSE (
	IF NOT EXIST "!SEARCH_PATH!" (
		ECHO ** ERROR: Specified directory "%SEARCH_PATH%" doesn't exist.
		EXIT /B 1
	)
	:: Remove trailing slashes
	IF "!SEARCH_PATH:~-1!"=="\" ( SET "SEARCH_PATH=!SEARCH_PATH:~0,-1!" )
)

IF NOT DEFINED NO_PROMPT (
	ECHO.
	CHOICE /M "** HOLD ON^! You're going to build cubemaps for all compiled maps in addon, this WILL take a long time, are you sure?"
	IF !ERRORLEVEL! NEQ 1 (
		ECHO Aborting batch cubemaps building...
		EXIT /B 0
	)
)

:: Looking for .bsp files in specified folder
FOR %%I IN ("!SEARCH_PATH!\*.bsp") DO (
	CALL "%SDKENV%\utils\build\build_cubemaps.bat" "%%~I" "%COMPILE_CONFIG%" "%~3" "%~4" || EXIT /B
)

:end
ECHO.
ECHO Batch cubemaps building finished.
ENDLOCAL
EXIT /B 0
