@ECHO OFF
::
:: Rolls back set environment for the shell
::
:: Usage by user:
::     >deactivate

:: Checking SDK environment
IF NOT DEFINED VPROJECT (
	ECHO VPROJECT was not defined, there is probably nothing to deactivate.
	EXIT /B 0
)

:: Rolling back the environment
IF DEFINED _OLD_PATH (
	SET "PATH=%_OLD_PATH%"
)
IF DEFINED _OLD_PROMPT (
	SET "PROMPT=%_OLD_PROMPT%"
)
SET "VPROJECT="
SET "_OLD_PATH="
SET "_OLD_PROMPT="

EXIT /B 0
