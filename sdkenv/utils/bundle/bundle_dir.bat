@ECHO OFF
SETLOCAL
::
:: Script for extracting content to separate folder
::
:: Usage:
::     CALL bundle_dir.bat [output_dir_path]
::
:: Parameters:
::     output_dir_path - optional output path, by default it is something like "%WORKDIR%\dist\%ADDON_NAME%"

:: What folders to check
SET "CONTENT_DIRS=expressions;maps;materials;media;models;particles;resource;scenes;scripts;sound"
:: What files to look for
SET "CONTENT_TYPES=.ani;.bik;.bsp;.cur;.dat;.jpg;.lmp;.lst;.mdl;.mov;.mp3;.nav;.pcf;.phy;.png;.res;.txt;.vcd;.vfe;.vfont;.vmt;.vtf;.vtx;.vvd;.wav"

SET "OUTDIR=%~dpnx1"
IF NOT DEFINED OUTDIR ( SET "OUTDIR=%WORKDIR%\dist\%ADDON_NAME%" )

IF NOT EXIST "%OUTDIR%\" (
	MKDIR "%OUTDIR%" || ( ECHO ** ERROR: Cannot create output directory "%OUTDIR%". & EXIT /B )
)

:: Copying content into separate folder
ECHO Extracting content to "%OUTDIR%", please wait...
FOR %%A IN ("%CONTENT_DIRS:;=" "%") DO (
	CALL :copy_files "%WORKDIR%\%%~A" || EXIT /B
)

ECHO Done content extracting.
ENDLOCAL
EXIT /B 0


::
:: Subroutines
::
:copy_files
	SETLOCAL EnableDelayedExpansion
	FOR /R "%~1" %%I IN ("*%CONTENT_TYPES:;=" "*%") DO (
		SET "SOURCE_PATH=%%~I"
		SET "OUTPUT_PATH=!SOURCE_PATH:%WORKDIR%=%OUTDIR%!"
		ECHO F | XCOPY "!SOURCE_PATH!" "!OUTPUT_PATH!" /Y >nul || ( ECHO ** ERROR: Cannot copy file "!SOURCE_PATH!" to "!OUTPUT_PATH!". & EXIT /B )
	)
	ENDLOCAL
	EXIT /B 0
