@ECHO OFF
SETLOCAL

:: Input check
SET "INPUT=%~dpnx1"
IF [%INPUT%] == [] (
	ECHO ** ERROR: Invalid input.
	EXIT /B 1
)

:: Optional parameters parsing
:loop
IF NOT "%~1"=="" (
	IF /I "%~1"=="/R" ( SET "RECURSIVELY=1" )
	IF /I "%~1"=="/C" ( SET "CLEANUP=1" )
	SHIFT & GOTO loop
)

IF EXIST "%INPUT%\" (
	CALL :convert_dir "%INPUT%" || EXIT /B
) ELSE (
	IF EXIST "%INPUT%" (
		CALL :convert_file "%INPUT%" || EXIT /B
	) ELSE (
		ECHO ** ERROR: "%INPUT%" doesn't exist!
	)
)

ENDLOCAL
EXIT /B 0


::
:: Subroutines
::
:convert_dir
	IF DEFINED RECURSIVELY (
		FOR /R "%~1" %%I IN (*.vtf) DO (
			CALL :convert_file "%%~I" || EXIT /B
		)
	) ELSE (
		FOR %%I IN ("%~1\*.vtf") DO (
			CALL :convert_file "%%~I" || EXIT /B
		)
	)
	EXIT /B 0

:convert_file
	SETLOCAL

	:: Check if input file is valid
	IF NOT "%~x1"==".vtf" (
		ECHO ** ERROR: Invalid VTF texture file.
		EXIT /B 1
	)

	SET "VTF_FILE=%~dpnx1"
	SET "TGA_FILE=%~dpn1.tga"
	SET "PFM_FILE=%~dpn1*.pfm"
	SET "OUTPUT_FILE=%~dp1_%~n1.txt"
	SET "PARAMS_FILE=%~dpn1.txt"

	ECHO.
	ECHO Working on "%VTF_FILE%"
	>"%OUTPUT_FILE%" "%VTF2TGA_EXE%" -i "%VTF_FILE%" -o "%TGA_FILE%" || (
		TYPE "%OUTPUT_FILE%"
		DEL /F "%OUTPUT_FILE%"
		ECHO ** ERROR: Cannot decompile texture "%VTF_FILE%".
		EXIT /B
	)
	TYPE "%OUTPUT_FILE%"

	:: Restore compile parameters file 
	>"%PARAMS_FILE%" CALL :parse_vtf2tga_output "%OUTPUT_FILE%" || ( ECHO ** ERROR: Cannot create file with compile parameters "%FILES_LIST%". & EXIT /B )
	DEL /F "%OUTPUT_FILE%" || EXIT /B

	IF DEFINED CLEANUP (
		ECHO.
		ECHO Cleaning up "%VTF_FILE%"
		DEL /F "%VTF_FILE%" || EXIT /B
	)
	
	ENDLOCAL
	EXIT /B 0

:parse_vtf2tga_output
	CALL :parse_format "%~1" || EXIT /B
	CALL :parse_frames "%~1" || EXIT /B
	CALL :parse_flags "%~1" || EXIT /B
	EXIT /B 0

:parse_format
	FINDSTR /I /R /C:"vtf format: DXT*" "%~1" >nul 2>nul || (
		ECHO nocompress 1
	)
	IF EXIST "%PFM_FILE%" (
		ECHO pfm 1
		ECHO pfmscale 1
	)
	EXIT /B 0

:parse_frames
	FOR /F "tokens=1-2 delims=:" %%A IN ('FINDSTR /B /R /C:"vtf numFrames*" "%~1"') DO (
		IF %%~B GTR 1 (
			CALL :display_frames "%%~B"
		)
	)
	EXIT /B 0

:display_frames
	SETLOCAL
	SET /A "END_FRAME=%~1 - 1"
	ECHO startframe 0
	ECHO endframe %END_FRAME%
	ENDLOCAL
	EXIT /B 0

:parse_flags
	SETLOCAL
	FOR /F "tokens=1-2 delims==" %%A IN ('FINDSTR /B /R /C:"TEXTUREFLAGS*" "%~1"') DO (
		IF /I "%%~B"=="true" (
			CALL :display_flag "%%~A"
		)
	)
	ENDLOCAL
	EXIT /B 0

:display_flag
	SETLOCAL
	CALL :resolve_flag "%~1" FLAG_NAME
	IF %FLAG_NAME% NEQ 0 (
		ECHO %FLAG_NAME% 1
	)
	ENDLOCAL
	EXIT /B 0

:resolve_flag
	SETLOCAL
	CALL :__resolve_flag %*

	:__resolve_flag
	GOTO :%~1 2>nul || GOTO :null

	:TEXTUREFLAGS_POINTSAMPLE
		SET "FLAG_NAME=pointsample"
		GOTO _end
	:TEXTUREFLAGS_TRILINEAR
		SET "FLAG_NAME=trilinear"
		GOTO _end
	:TEXTUREFLAGS_CLAMPS
		SET "FLAG_NAME=clamps"
		GOTO _end
	:TEXTUREFLAGS_CLAMPT
		SET "FLAG_NAME=clampt"
		GOTO _end
	:TEXTUREFLAGS_CLAMPU
		SET "FLAG_NAME=clampu"
		GOTO _end
	:TEXTUREFLAGS_BORDER
		SET "FLAG_NAME=border"
		GOTO _end
	:TEXTUREFLAGS_ANISOTROPIC
		SET "FLAG_NAME=anisotropic"
		GOTO _end
	:TEXTUREFLAGS_HINT_DXT5
		SET "FLAG_NAME=dxt5"
		GOTO _end
	:TEXTUREFLAGS_SRGB
		SET "FLAG_NAME=srgb"
		GOTO _end
	:TEXTUREFLAGS_NORMAL
		SET "FLAG_NAME=normal"
		GOTO _end
	:TEXTUREFLAGS_NOMIP
		SET "FLAG_NAME=nomip"
		GOTO _end
	:TEXTUREFLAGS_NOLOD
		SET "FLAG_NAME=nolod"
		GOTO _end
	:TEXTUREFLAGS_ALL_MIPS
		SET "FLAG_NAME=allmips"
		GOTO _end
	:TEXTUREFLAGS_PROCEDURAL
		SET "FLAG_NAME=procedural"
		GOTO _end
	:TEXTUREFLAGS_RENDERTARGET
		SET "FLAG_NAME=rendertarget"
		GOTO _end
	:TEXTUREFLAGS_NODEBUGOVERRIDE
		SET "FLAG_NAME=nodebug"
		GOTO _end
	:TEXTUREFLAGS_SINGLECOPY
		SET "FLAG_NAME=singlecopy"
		GOTO _end
	:null
		SET "FLAG_NAME=0"
		GOTO _end

	:_end
	ENDLOCAL & SET "%~2=%FLAG_NAME%"
	EXIT /B 0
