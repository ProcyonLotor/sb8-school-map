@ECHO OFF
::
:: Main script that sets up SDK environment
:: Environment variables are:
::     SDKENV     - path to current addon's "sdkenv" folder
::     ADDON_NAME - addon folder name
::     WORKDIR    - path to current addon
::     VPROJECT   - path to addon's game
::     GAMEPATH   - path to game directory (with hl2.exe)
::     *_EXE      - addon game's tools
::
:: Usage:
::     CALL set_sdk_env.bat

:: Set SDKENV path
CALL :norm_path "%~dp0.." SDKENV
:: Set WORKDIR path
CALL :norm_path "%~dp0..\.." WORKDIR
:: Set addon name
FOR %%I IN ("%WORKDIR%") DO ( SET "ADDON_NAME=%%~nxI" )
:: Set and validate VPROJECT
CALL :set_vproject "%WORKDIR%\.vproject" VPROJECT || EXIT /B
IF NOT EXIST "%VPROJECT%\gameinfo.txt" (
	ECHO ** Folder "%VPROJECT%" has no "gameinfo.txt" - VPROJECT is invalid!
	SET "VPROJECT="
	EXIT /B 1
)
:: Set path to game directory (with hl2.exe)
CALL :norm_path "%VPROJECT%\.." GAMEPATH

:: Set tools paths
CALL :set_env_file "%SDKENV%\tools.txt" || EXIT /B

:: Set other variables (if present)
IF EXIST "%WORKDIR%\.env" (
	CALL :set_env_file "%WORKDIR%\.env" || EXIT /B
)

:: Checking set environment 
CALL "%SDKENV%\utils\check_env_integrity.bat" || EXIT /B

:: Sctipt end
EXIT /B 0


::
:: Subroutines
::

:: Normalize path
:norm_path
	SETLOCAL
	PUSHD "%~1"
	SET "NORMALIZED_PATH=%CD%"
	POPD
	ENDLOCAL & SET "%~2=%NORMALIZED_PATH%"
	EXIT /B 0

:: Check and set VPROJECT
:set_vproject
	SETLOCAL
	IF EXIST "%~1" (
		SET /P _VPROJECT=<"%~1"
	) ELSE (
		ECHO ** ERROR: Local VPROJECT is not set, try to run "set_vproject.bat" first.
		EXIT /B 1
	)
	ENDLOCAL & SET "%~2=%_VPROJECT%"
	EXIT /B 0

:: Set environment variables from a file
:set_env_file
	SET "ENV_FILE=%~dpnx1"
	IF NOT EXIST "%ENV_FILE%" (
		ECHO ** ERROR: File with environment variables ^("%ENV_FILE%"^) is not found.
		EXIT /B 1
	)

	FOR /F "tokens=*" %%I IN ('FINDSTR /B /V /R /C:" *#" "%ENV_FILE%"') DO (
		:: Using "CALL SET" allows for variables expansion inside a string
		CALL SET "%%~I" || EXIT /B 1
	)

	:: Unset local variable
	SET "GAMEPATH="
	EXIT /B 0
