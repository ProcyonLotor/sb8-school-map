@ECHO OFF
::
:: Wrapper script for textures compiling, accepts both .tga and .txt
::
:: Usage:
::     CALL build_model.bat <texture_file | texture_config>
::
:: Parameters:
::     texture_file   - source texture file (TGA)
::     texture_config - texture file compile config (TXT)

:: Check if input is one of valid files
IF NOT "%~x1"==".tga" (
	IF NOT "%~x1"==".txt" (
		ECHO ** ERROR: Invalid texture file.
		EXIT /B 1
	)
)

SETLOCAL
SET "INPUT_FILE=%~dpnx1"
SET "INPUT_DIR=%~dp1"

:: Nobody like trailing slashes in output path, Vtex especially
IF "%INPUT_DIR:~-1%"=="\" ( SET "INPUT_DIR=%INPUT_DIR:~0,-1%" )

:: Figuring out where to compile texture to
:: If input file has "materialsrc", then we probably can resolve its place in final "materials" folder
IF /I "%INPUT_DIR:materialsrc=%"=="%INPUT_DIR%" (
	ECHO.
	ECHO ** WARNING: Cannot resolve texture's output folder, compiling to the same folder...
	GOTO same_folder
) ELSE (
	ECHO.
	ECHO Compiling to working directory...
	GOTO working_dir
)

:same_folder
	CALL vtex -dontusegamedir "%INPUT_FILE%" || EXIT /B
	GOTO end

:working_dir
	:: Figuring out texture path realtive to "materials" folder
	FOR %%A IN ("%INPUT_DIR:\=" "%") DO (
		CALL SET "TEX_PATH=%%TEX_PATH%%\%%~A"
		IF /I "%%~A"=="materialsrc" ( CALL SET "TEX_PATH=" )
	)
	CALL vtex -mkdir -outdir "%WORKDIR%\materials%TEX_PATH%" "%INPUT_FILE%" || EXIT /B
	GOTO end

:end
ENDLOCAL
EXIT /B 0
