@ECHO OFF
SETLOCAL
::
:: Run the game with a config
::
:: Usage by user:
::     >run [/?]
::     >run game [game_launch_options]
::     >run tools [game_launch_options]
::     >run map <map_name> [game_launch_options]

:: Routing
SET "RUN_CONFIG=%~1"
IF NOT DEFINED RUN_CONFIG ( GOTO help )
IF "%RUN_CONFIG%"=="/?" ( GOTO help )
GOTO %RUN_CONFIG% 2>nul || ECHO ** ERROR: Unknown run config "%RUN_CONFIG%".

::
:: Run configurations
::
:game
	ECHO Starting Game in Developer mode...
	ECHO Executing "%GAME_EXE%" -novid -dev -console -allowdebug -game "%VPROJECT%" %2 %3 %4 %5 %6 %7 %8 %9
	START "" "%GAME_EXE%" -novid -dev -console -allowdebug -game "%VPROJECT%" %2 %3 %4 %5 %6 %7 %8 %9
	GOTO end

:tools
	ECHO Starting Game in Tools mode...
	ECHO Executing "%GAME_EXE%" -novid -dev -tools -nop4 -allowdebug -game "%VPROJECT%" %2 %3 %4 %5 %6 %7 %8 %9
	START "" "%GAME_EXE%" -novid -dev -tools -nop4 -allowdebug -game "%VPROJECT%" %2 %3 %4 %5 %6 %7 %8 %9
	GOTO end

:map
	ECHO Starting a Map...
	ECHO Executing "%GAME_EXE%" -novid -dev -console -allowdebug -game "%VPROJECT%" +map "%~2" %3 %4 %5 %6 %7 %8 %9
	START "" "%GAME_EXE%" -novid -dev -console -allowdebug -game "%VPROJECT%" +map "%~2" %3 %4 %5 %6 %7 %8 %9
	GOTO end

:help
	ECHO.
	ECHO Run the game with a config
	ECHO.
	ECHO Usage:
	ECHO     run ^<config^> [additional_options]
	ECHO.
	ECHO Examples:
	ECHO     run game [game_launch_options]
	ECHO     run tools [game_launch_options]
	ECHO     run map ^<map_name^> [game_launch_options]
	ECHO.
	ECHO Configs:
	ECHO     game  - runs game in developer mode
	ECHO     tools - runs game in tools(commentary, particles, etc.) mode
	ECHO     map   - runs game in developer mode and loads specified map
	GOTO end

:end
ENDLOCAL
