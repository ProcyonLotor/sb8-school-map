@ECHO OFF
::
:: Convert binary dmx files to keyvalues2
::
:: Usage by user:
::     >bin2kv2 <binary_dmx_file> [/R]

:: Input check
SET "INPUT=%~dpnx1"
IF /I "%~2"=="/R" ( SET "RECURSIVELY=1" )
IF [%INPUT%] == [] (
	ECHO ** ERROR: Invalid input.
	EXIT /B 1
)

ECHO.
IF EXIST "%INPUT%\" (
	ECHO Converting direcotry "%INPUT%"
	CALL :convert_dir "%INPUT%" || EXIT /B
) ELSE (
	IF EXIST "%INPUT%" (
		ECHO Converting file "%INPUT%"
		CALL :convert_file "%INPUT%" || EXIT /B
	) ELSE (
		ECHO ** ERROR: "%INPUT%" doesn't exist!
	)
)

ENDLOCAL
EXIT /B 0


::
:: Subroutines
::
:convert_dir
	IF DEFINED RECURSIVELY (
		FOR /R "%~1" %%I IN (*.dmx) DO (
			CALL :convert_file "%%~I" || EXIT /B
		)
	) ELSE (
		FOR %%I IN ("%~1\*.dmx") DO (
			CALL :convert_file "%%~I" || EXIT /B
		)
	)
	EXIT /B 0

:convert_file
	:: Check if file is valid
	IF NOT "%~x1"==".dmx" (
		ECHO ** ERROR: Invalid DMX file.
		EXIT /B 1
	)
	:: At least some output
	ECHO dmxconvert -i "%~1" -oe keyvalues2
	CALL dmxconvert -i "%~1" -oe keyvalues2 || EXIT /B
	EXIT /B 0
