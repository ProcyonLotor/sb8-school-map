@ECHO OFF
SETLOCAL
::
:: Script for bundling content into VPK file
::
:: Usage:
::     CALL bundle_vpk.bat [output_file]
::
:: Parameters:
::     output_file - optional output file, by default it is "%WORKDIR%\dist\%ADDON_NAME%.vpk"

:: What folders to pack
SET "CONTENT_DIRS=expressions;maps;materials;media;models;particles;resource;scenes;scripts;sound"
:: What files to look for
SET "CONTENT_TYPES=.ani;.bik;.bsp;.cur;.dat;.jpg;.lmp;.lst;.mdl;.mov;.mp3;.nav;.pcf;.phy;.png;.res;.txt;.vcd;.vfe;.vfont;.vmt;.vtf;.vtx;.vvd;.wav"

IF NOT "%~1"=="" (
	SET "OUTDIR=%~dp1"
	SET "VPK_FILE=%~dpnx1"
) ELSE (
	SET "OUTDIR=%WORKDIR%\dist"
	SET "VPK_FILE=%WORKDIR%\dist\%ADDON_NAME%.vpk"
)

IF NOT EXIST "%OUTDIR%\" (
	MKDIR "%OUTDIR%" || ( ECHO ** ERROR: Cannot create output directory "%OUTDIR%". & EXIT /B )
)

SET "VPK_LIST=%WORKDIR%\vpk_list.txt"

:: Preparing a Response File
>"%VPK_LIST%" CALL :sweep_dirs || ( ECHO ** ERROR: Cannot create a Response File "%VPK_LIST%". & EXIT /B )
:: Packing content into VPK file
ECHO Packing content in "%VPK_FILE%", please wait...
:: VPK must be run relative to the directory that corresponds to paths in the Response File
PUSHD "%WORKDIR%"
CALL vpk a "%VPK_FILE%" @"%VPK_LIST%" || ( POPD & EXIT /B )
POPD
:: Clean up Response File
DEL /F "%VPK_LIST%" || EXIT /B

ECHO Done VPK packing.
ENDLOCAL
EXIT /B 0


::
:: Subroutines
::
:sweep_dirs
	FOR %%A IN ("%CONTENT_DIRS:;=" "%") DO (
		CALL :find_files "%WORKDIR%\%%~A" || EXIT /B
	)
	EXIT /B 0

:find_files
	SETLOCAL EnableDelayedExpansion
	FOR /R "%~1" %%I IN ("*%CONTENT_TYPES:;=" "*%") DO (
		SET "FILE_PATH=%%~I"
		ECHO !FILE_PATH:%WORKDIR%\=!
	)
	ENDLOCAL
	EXIT /B 0
